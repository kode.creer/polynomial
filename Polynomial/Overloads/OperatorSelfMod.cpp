//
//  PolynomialOperatorOverload.cpp
//  THE_POLONOMIAL
//
//  Created by Kode Creer on 11/3/18.
//  Copyright © 2018 Kode Creer. All rights reserved.
//

#include "/Users/kodecreer/Documents/C++/DSandAlgs/THE_POLONOMIAL/THE_POLONOMIAL/Polynomial.h"
#include <unistd.h>

//Stuff modified from inside the class
Polynomial& Polynomial::operator = (double a0){
    data[0] = a0;
    return *this;
}

//reference return
Polynomial& Polynomial::operator = (const Polynomial& source){

    return *this;
}

void Polynomial::operator += (const Polynomial& source){
    
    used = source.size() > size() ? source.size() : size();
    
    for(size_t i = 0; i < source.size(); ++i){
        data[i] += source.data[i];
    }
}
void Polynomial::operator *= (const Polynomial& source){

    const int size1 = int(size());
    const int size2 = int(source.size());
    
    used += ((size1 + size2)-1 - used);
    for (int i = size1-1; i >= 0; --i) {
        for (int b = size2-1; b >= 0; --b) {
            double value;
            
            value = data[i] * source.data[b];
            
            data[i + b] += value;
        }
    }

}
void Polynomial::operator -= (const Polynomial& source){
    sizeT max = source.size() > size() ? source.size() : size();
    
    for (size_t i = 0; i < max; ++i) {
        data[i] -= source.data[i];
    }
    
}

void Polynomial::operator /= (const Polynomial& source){
//    sizeT max = source.size() < size() ? source.size() : size();
//
//    for (sizeT i = 0; i < max; ++i) {
//
//        data[i] /= source.data[i];
//
//    }
    
}

